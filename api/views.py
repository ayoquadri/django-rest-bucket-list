from django.shortcuts import render
from rest_framework import generics, permissions
from .serializers import BucketListSerializer
from .models import BucketList
from .permissions import IsOwner
# Create your views here.

#The ListCreateAPIView is a generic view which provides GET (list all) and POST method handlers
class CreateView(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api"""
    queryset = BucketList.objects.all()
    serializer_class = BucketListSerializer
    #  We could have used IsAuthenticatedOrReadOnly which permits unauthenticated users if
    #   the request is one of the "safe" methods (GET, HEAD and OPTIONS). But we want full security
    permission_classes = (permissions.IsAuthenticated,IsOwner)

    def perform_create(self,serializer):
        """Save the post data when creating a new bucketlist."""
        serializer.save(owner=self.request.user)    
        
#RetrieveUpdateDestroyAPIView is a generic view that provides GET(one), PUT, PATCH and DELETE method handlers.
class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http GET, PUT and DELETE requests. """
    queryset = BucketList.objects.all()
    serializer_class = BucketListSerializer
